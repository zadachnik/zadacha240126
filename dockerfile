FROM node:16-alpine as build

ARG GAME_NAME=$GAME_NAME
WORKDIR /$GAME_NAME
COPY $GAME_NAME/package*.json .
COPY $GAME_NAME/webpack.config.js .
COPY $GAME_NAME/src ./src
RUN npm install --include=dev &&\
    npm run build &&\
    npm cache clean --force

FROM nginx:1.25-alpine3.18-slim

ARG GAME_NAME=$GAME_NAME
EXPOSE 80
WORKDIR /usr/share/nginx/html
COPY --from=build /$GAME_NAME/dist .
